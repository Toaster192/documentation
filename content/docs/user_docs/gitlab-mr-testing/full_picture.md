---
title: Full picture of CKI merge request testing
description: Start here if you are new to the merge request workflow
linkTitle: Full picture
weight: 10
---

CKI can test kernels living in GitLab as well. Currently this setup is only
deployed for GitLab.com, and as such this guide will reference it directly.
Administrative setup description can be found under the documentation for
[pipeline triggering], these docs focus on the kernel engineer side.

> Throughout this guide, contacting CKI team is mentioned several times. You
> can do so by tagging `@cki-project` group on `gitlab.com`, sending an email
> to `cki-project@redhat.com` or pinging us on `#kernelci` IRC channel. You
> can also contact individual team members of the project, who are listed in
> the [members] tab of the project.

## Trusted and external contributors

Contributors are split into two groups - *trusted* and *external*. In general,
to be considered a trusted contributor you need to be a [project member]. As a
project member, you get access to trigger testing pipelines in specific
[CKI pipeline] projects. The pipeline will run in the project and branch linked
from the `trigger` section in the `.gitlab-ci.yml`, and the pipeline status will
be reflected on the merge request. Configuration options for the pipeline can be
overriden by changing the `variables` section of the `.gitlab-ci.yml` file. This
can be useful if e.g. your changes modify the target to create `srpm`, like
`kernel-ark` switched from `make rh-srpm` to `make dist-srpm`. Check out our
[configuration option] documentation for the full list of available options.

If you are an external contributor, the pipelines will fail with permission
issues. Two common messages are `Downstream project could not be found` (in
case you don't have permissions to even view the pipeline project) and `No
permissions to trigger downstream pipeline` (in case the project is public and
thus you only lack trigger permissions). These messages can be seen e.g. when
hovering over the failed trigger job in the pipeline view.

![Example of failed pipeline due to permissions](no-permissions.png)

In this case, a bot will leave a comment explaining which project membership is
required for automatic testing. It will also trigger testing pipelines with
predefined configuration options and link them in another comment. The comment
with pipeline status will be updated as the pipelines finish. The bot will
create new pipelines after any changes to your merge request.

<!-- markdownlint-disable no-blanks-blockquote -->
<!-- Yes, these are two separate quotes on purpose -->
> If you believe you *should* have permissions to run testing yourself, ask the
> project maintainers. If they agree, they will grant you permissions. Push to
> your branch after the permissions are granted to test them out.

> Note that only projects accepting external contributions have the bot set up.
> If you submitted a merge request to a **private project** and don't have
> permissions to trigger testing, this is likely by accident. Contact the
> project maintainers to get the issue resolved.
<!-- markdownlint-restore -->

While external contributors can submit changes to `gitlab-ci.yml` file, these
changes are **not** reflected in the created pipelines due to security concerns
and only take effect on subsequent pipelines after the changes are merged.
Configuration options used for external contributors' pipelines are stored
outside of kernel sources and any changes to them need to be ACKed by either
kernel maintainers or CKI team.

## What will a high level CI flow look like?

If you want any kernels built or tested, you have to submit a merge request.
This is used as an explicit "I want CI" request as opposed to testing any pushes
to git branches, to conserve resources when no testing is required. **Merge
requests marked as "WIP:" or "Draft:" are also tested**. These are useful e.g.
if your changes are not yet ready or are a throwaway (e.g. building a kernel
with a proposed fix to help support engineers). The prefix indicates to
reviewers they don't have to spend their time looking into the merge request.

Once you submit a merge request, a CI pipeline will automatically start. If you
want to, you can follow the pipeline along (e.g. by clicking on the "Detached
merge request pipeline" link on the merge request web page) to check progress.
The MR CI process utilizes [multi-project pipelines], and thus you'll see a very
condensed view.

![Multi-project pipeline view](multiproject.png)

To view the full pipeline, click on the arrow on the right side of the box that
says "Multi-project".

![Full Multi-project pipeline](multiproject-full.png)

Note that you may have to scroll to the side to see the full pipeline
visualisation!

Full testing will likely take a few hours. If the pipeline fails, you'll
automatically get an email from GitLab. This can be changed in your
[notification settings]. The pipeline can fail for different reasons and most
of them require your attention, so we suggest to check the status periodically
if you decide to turn the emails off. In case you opt to receive the emails,
this is how the email will look like:

![Failed email example](failed-notification.png)

Notice the clickable pipeline ID in the email ("Pipeline #276757970 triggered"
in the example screenshot). Clicking on it will bring you to the condensed
multiproject pipeline view, as shown in the first screenshot in this section.
You also get links pointing to your merge request ("!455" in the example) and
the commit which the pipeline was triggered for ("5ea6959d" in the example).
You can get to the pipeline from these links as well, however there's no need
to use them since the direct link is available.

CKI team has implemented [automatic retries] to handle intermittent
infrastructure failures and outages. Such failed jobs will be automatically
retried using exponential back off and thus **no action is required from you on
infrastructure failures**. In general, **any other failures are something you
should check out**. Follow the information in the job output to find out what
happened and fix up the merge request if needed.

> If you think there's a CI or test bug, have any issues figuring out what's
> going on, or need to add new build dependencies or features please contact
> the CKI team or the appropriate test maintainer.

### Real time kernel checks

In some cases, there will be a second pipeline named `realtime_check` available
for the merge request as well. The purpose of the pipeline is to check if your
merge request is compatible with the real time kernel. Failures in this pipeline
are **not** blocking. They are only an indicator for the real time kernel
maintainers that either their branch is out of sync with the regular kernel, or
that they will run into problems when merging your changes into the real time
tree. *You can ignore the `realtime_check` pipeline, the real time kernel team
will contact you if they need to follow up.*

![Regular and real time kernel pipelines](two-pipelines.png)

If the regular pipeline passes but the real time kernel check fails (as shown
in the screenshot above), you will see a different marking in the GitLab web UI
of your merge request:

![Allowed to fail](allowed-to-fail.png)

Automation checking the bare pipeline status is not affected by the
`realtime_check` failure.

### Debug kernel testing

CKI does *not* build or test debug kernels for MRs. The functionality is
implemented, however it is currently blocked by the maximum artifacts size on
the instance (1G on gitlab.com) which is not sufficient to store a debug kernel
repository. The CKI team is planning on utilizing an external artifact storage
to work around this limitation, however that requires a significant amount of
work. For now, please use build systems like Koji/Brew/COPR to build debug
kernels. We'll update this page when the functionality is available.

## Testing outside of CI pipeline

Not all kernel tests are onboarded into CKI and thus they can't run as part of
the merge request pipeline. Please consider automating and [onboarding] as many
kernel tests as possible to increase the test coverage and avoid having to
manually trigger the tests every time they are needed.

For testing outside of CI pipelines, developers and QE and encouraged to reuse
kernels built by CKI. Kernel tarballs or RPM repository links (usable by `dnf`
and `yum`) -- depending on which are applicable -- are provided in the
`publish` job output.

The artifacts are available for at least 6 weeks after job completion.

There are two URLs linked in the job output:

![RPM repository linked in the job output](artifacts.png)

The first URL links to the job artifact page in the GitLab web interface.
Access might be limited by GitLab authentication and require credentials. The
second URL links to a yum/dnf repo file that does *not* require any
authentication. Both links are also provided to QE in the Bugzilla associated
with the MR.

### UMB messages for built kernels

CKI is providing UMB messages for each built kernel. No extra authentication to
access the provided kernel repositories is needed. Note that merge request
access (e.g. to check the changes or add a comment with results) *will* require
authentication and you may want to set up an account to provide results back on
the merge request. If you'd like to start using the UMB messages or have any
requests about them, please check out the [messenger repository] or open an
issue there.

**Note that no UMB messages are going to be sent for any embargoed content as
doing so would make the sensitive information available company wide.** If you
are assigned to test any sensitive content, you need to grab the kernel repo
files from the merge request pipelines (publish jobs) manually.

UMB testing will also support extended test sets. These are meant to substitute
scratch build testing done by QE/CI and will *not* run automatically but an
explicit request will be required. Please coordinate with your QE/CI
counterparts and CKI if you want to enable such testing.

### Testing RHEL content outside of the firewall

There are cases where RHEL kernel binaries need to be provided to partners for
testing. As private kernel repositories are only available inside Red Hat,
manual steps similar to the original flow using Brew are needed here.

To download the binaries, open the `publish` for the right architecture(s). On
the right side, there will be a button to download the job artifacts (see the
screenshot of this area in the section below). This will get you a zipped local
copy of *all* the artifacts, including information about the internal pipeline
state, diff of the kernel changes and, of course, the kernel repository. You
may now unzip the archive and share the directory containing the kernel
repository (or the specific binaries from the directory) the same way you did
previously.

### Targeting specific hardware (not available yet)

CKI automatically picks any supported machine fitting the test requirements.
This setup is good for default sanity testing, but doesn't work if you need to
run generic tests on specific hardware. One such example is running generic
networking tests on machines with specific drivers that are being modified. The
team is working on extending the patch analysis tool to add device and driver
filters based on kernel changes, so that targeted hardware is picked where
applicable inside the CKI pipeline. Note that this mapping needs to be added to
specific release/driver/architecture cases manually as first it needs to be
verified that machines fitting all filters are available for CKI use. **If you
feel a commonly used mapping combination is missing and would be useful to
include automatically please reach out to the CKI team.**

For cases not covered by the mapping mentioned above, CKI will provide a bot to
allow any trusted contributor to trigger extra CKI testing on specific machines.
The bot will leave a comment on every merge request outlining how to trigger the
testing. Beaker driver filter and specific hostname usage will be supported.
This bot will likely handle extended UMB testing mentioned earlier as well.
*Note that this testing is not a substitute for passing merge request pipelines
as generic sanity testing is still required to make sure there are no unexpected
side effects of the changes.*

## Debugging and fixing failures - more details

To check failure details, click on the pipeline jobs marked with `!` or a red
`X`. Those are the ones that failed. You'll see output of the pipeline, with
some notes about what's being done. Some links (like with the aforementioned
kernel repositories) may also be provided. Extra logs (e.g. full build output)
will usually be available in the job artifacts. Check those out to identify the
source of the problem if the job output itself is not sufficient. In the web
view, job artifacts are available at the right side.

![Links to retrieve artifacts](artifacts-link.png)

![Job artifacts](job-artifacts.png)

Fix any issues that look to be introduced by your changes and push new versions
(or even debugging commits). Testing will be automatically retried every time.

As mentioned previously, no steps are needed for infrastructure failures as
affected jobs will be automatically retried. Infrastructure issues include
(but are not limited to) e.g. pipeline jobs failing to start, jobs crashing due
to lack of resources on the node they ran on, or container registry issues. If
in doubt, feel free to retry the job yourself. You can find the retry button
right above the artifacts links, as shown in the screenshot above.

> Note the retry button doesn't pull in any new changes (neither kernel, nor
> CKI) and retries the original job *as it was before*. This is to provide a
> semi-reproducible environment and consistency within the pipeline.
> Essentially, *only transient infrastructure issues can be handled by
> retrying*. To pull in any new changes, you need to trigger a new pipeline by
> either pushing to your branch or clicking on the `Run pipeline` button in the
> pipeline tab on your merge request.

### Test failures

Test output and logs are be available in the test stage jobs. If you're having
issues finding out the failure reason, please reach out to the test maintainers.
Test maintainer information is directly available in the failed test job logs.
Name and email contacts are be available for all maintainers. If the maintainers
provided CKI with their gitlab.com usernames, these are logged as well so you
can tag them directly on your merge request.

![Waived failed test information in the job logs](failed-waived-test.png)

In the example above you can see the test status, maintainer information and
direct links to the logs of the failed test. In this case, the test is waived
and thus its status does not cause the test job to fail. Tests which are not
waived say so in the message and the message is in red letters, as you can see
in the example below. The rest of the message is same.

![Failed and not waived test](failed-test-message.png)

#### Known test failures / issues (not available yet)

Tests may fail due to already existing kernel or test bugs or infrastructure
issues. Unless they are crashing the machines, we'll keep the tests enabled for
two reasons:

- To not miss any new bugs they could detect
- To make QE's job easier with fix verification; if a kernel bug is fixed the
  test will stop failing and it will be clearly visible

After testing finishes, the results and logs are submitted into our testing
database and checked for matches of any already known issues. If a known issue
is detected, the test failure is marked as such. Pipelines containing only known
failures and no other issues will be considered passing.

It's both test maintainers' and developers' / maintainers' responsibility to
submit known issues into the database. If a new issue is added to the database,
all previously submitted untagged failures are automatically rechecked and
marked in the database (not in the existing CI pipelines!) if they match, and
the issue will be correctly recognized on any future CI runs. Please reach out
to the CKI team for permissions to submit new issues.

### CI pipeline needs to be changed

1. **Your changes are adding new build dependencies.** New packages can't be
   installed ad-hoc, they need to be added to the builder container. If you can,
   please submit a merge request with the new dependency to the [container
   image repository].

2. **Your changes require pipeline configuration changes.** This is the case of
   the `make dist-srpm` example from above. Check out the [configuration option]
   documentation and adjust the `.gitlab-ci.yml` file.

3. **Your changes are incompatible with the current process and the pipeline
   needs to be adjusted to handle it, a new feature is requested, or a bug is
   detected.** This should rarely happen but in case it does, please contact
   the CKI team to work out what's needed.

If you need any help or assistance with any of the steps, feel free to contact
the CKI team!

## Future plans

Once the new workflow is well established, merge requests will start to be
blocked from being integrated on two occasions. Of course exceptions can be
evaluated on a case by case basis and emergencies with explicit approvals.

### Blocking on missing targeted testing

Every change needs to have a test associated with it (or at least one that
exercises the area) to verify it works properly. You can find whether such tests
are already onboarded into CKI via the targeted testing stats in the pipeline
logs.

Please work with your QE counterparts to create and onboard tests so your
changes can be properly tested.

In case no targeted test is detected, integration of the merge request will
require an explicit approval and potentially also manual verification. Targeted
hardware testing and extended UMB testing *may* be used to override the missing
flag, but as this requires a manual step, please still consider including as
many tests as possible directly!

### Blocking on failing CI pipelines

No changes introducing new problems should be integrated into the kernel. All
observed failures need to be related to known issues, if it's not a known
failure it needs to be fixed before the merge request can be included.

[members]: https://gitlab.com/groups/cki-project/-/group_members
[pipeline triggering]: ../../hacking/operations/pipeline-triggering.md#kernels-living-in-gitlabcom
[project member]: https://docs.gitlab.com/ee/user/project/members/
[CKI pipeline]: https://gitlab.com/redhat/red-hat-ci-tools/kernel/
[configuration option]: ../configuration.md
[notification settings]: https://docs.gitlab.com/ee/user/profile/notifications.html#notifications-on-issues-merge-requests-and-epics
[automatic retries]: https://gitlab.com/cki-project/pipeline-herder
[onboarding]: ../../test-maintainers/onboarding.md
[container image repository]: https://gitlab.com/cki-project/containers
[multi-project pipelines]: https://docs.gitlab.com/ee/ci/multi_project_pipelines.html
[kpet-db]: https://gitlab.com/cki-project/kpet-db
[messenger repository]: https://gitlab.com/cki-project/umb-messenger
