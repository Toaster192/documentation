---
title: Frequently Asked Questions
description: Have a specific question and no time to read the detailed docs?
linkTitle: FAQ
weight: 20
---

1. [I got an email about a failed pipeline, how do I find the failure?](#i-got-an-email-about-a-failed-pipeline-how-do-i-find-the-failure)
2. [What is `realtime_check` and what should I do when it fails?](#what-is-realtime_check-and-what-should-i-do-when-it-fails)
3. [How do I send built kernels to partners?](#how-do-i-send-built-kernels-to-partners)
4. [A test failed and I don't understand why!](#a-test-failed-and-i-dont-understand-why)
5. [How do I retry a job?](#how-do-i-retry-a-job)
6. [How do I retry a pipeline?](#how-do-i-retry-a-pipeline)

### I got an email about a failed pipeline, how do I find the failure?

![Failed email example](failed-notification.png)

Click on the pipeline ID in the email (in the "Pipeline #276757970 triggered"
part in the example). This will bring you to the generic pipeline view:

![Multi-project pipeline view](multiproject.png)

Click on the right arrow to unpack the pipeline:

![Full multi-project pipeline](multiproject-full.png)

> Note that you may have to scroll to the side to see the full pipeline
> visualisation and find the failures!

Any failed jobs will be marked with a red cross mark. Click on them and follow
the output to find out what happened. If the job output is not sufficient,
complete logs are available in the job artifacts, which you can browse in the
web UI or download for local use:

![Links to retrieve artifacts](artifacts-link.png)

For more information, see the [detailed] debugging guide.

### What is `realtime_check` and what should I do when it fails?

The checks are in place to give the real time kernel team a heads up about
conflicts with the real time kernel branches. No action from the developers is
required at this stage and failures are **not** blocking. The real time kernel
team will contact you if they need to follow up.

### How do I send built kernels to partners?

Open the `publish` job of your desired architecture. On the right side, there
will be an area with the job summary and artifacts, similar to this example from
a `build` job:

![Links to retrieve artifacts](artifacts-link.png)

Click on the `Download` button at the bottom to download the artifacts locally.
Extract the zipped archive and pick either the directory with the `dnf/yum`
kernel repository, or specific binaries from it. You can forward these to
partners the same way you used to forward builds before.

### A test failed and I don't understand why!

If a test failed, links to test logs and maintainer information will be printed
in the test job:

![Waived failed test information in the job logs](failed-waived-test.png)

You can contact them if you can't figure out the failure reason yourself.

### How do I retry a job?

Jobs can be retried from both the pipeline overview and specific job views. In
the pipeline overview, you can click on the circled double arrows to retry
specific jobs:

![Full multi-project pipeline](multiproject-full.png)

If you already have a specific job open, click on the `Retry` button on the
right side:

![Job details](artifacts-link.png)

### How do I retry a pipeline?

Go into the `Pipelines` tab on your merge request and click on the green
`Run pipeline` button:

![Run pipeline](pipelines-tab.png)

[detailed]: full_picture/#debugging-and-fixing-failures---more-details
