---
title: Renew expired UMB certificates
linkTitle: Renew UMB certificates
description: How to renew the certificates used to authenticate against shared services, such as UMB.
---

## Problem

There are 2 service accounts used to access UMB:

1. `cki-amqp-bridge`: Read only permission.

1. `cki-umb-messenger`: Write permission on the `VirtualTopic.eng.cki.ready_for_test`
   topic.

These service accounts use certificates to authenticate, which need to be renewed
   every 1 year.

## Useful Links

1. [Accessing Shared Services]

1. [UMB Getting Started]

1. [How to: Request UMB Certificates]

1. [INC1643166](https://redhat.service-now.com/surl.do?n=INC1643166): Service
   Account creation ticket.

1. [RITM0854859](https://redhat.service-now.com/surl.do?n=RITM0854859): UMB
   authorization ticket.

## Steps

The instructions can be found on the Step 3 of [Accessing Shared Services].

1. Locally checkout the [IT-IAM Utilities] repository.

1. Using the `PKI/get_rhcs_app_cert.sh` script, generate the certificates for
   all the accounts. The script will ask for the service accounts password,
   get it from the [credentials repository].

   > Note: It's important to execute the script from inside the `PKI` folder
   > otherwise it won't work 🤦
   >
   > Note2: Non production service accounts are different service accounts,
   > but the script automatically prepends `nonprod-` to the name when using
   > the nonprod option, so you'll need to remove it from the cmd call.
   >
   > Note3: It is recommended to run the command inside a container as the script
   > needs some dependencies.
   >
   > Note4: The generated certificates are left in /tmp, so mount it as a
   > volume to access them from outside of the container easily.

   <!-- markdownlint-disable line-length -->
   ```bash
   $ git clone https://gitlab.corp.redhat.com/it-iam/utility
   $ podman run --rm -it -v $PWD/utility:/utility $PWD/output:/tmp registry.gitlab.com/cki-project/containers/base
   (container) $ dnf install -y certmonger openldap-clients openssl java-11-openjdk
   (container) $ cd /utility/PKI
   (container) /utility/PKI $ ./get_rhcs_app_cert.sh cki-amqp-bridge prod
   (container) /utility/PKI $ ./get_rhcs_app_cert.sh cki-amqp-bridge nonprod
   (container) /utility/PKI $ ./get_rhcs_app_cert.sh cki-umb-messenger prod
   (container) /utility/PKI $ ./get_rhcs_app_cert.sh cki-umb-messenger nonprod
   ```
   <!-- markdownlint-restore -->

1. Update the certificates on the [credentials repository] overwriting the
   existing files in the `certs/` path.

   <!-- markdownlint-disable line-length -->
   ```bash
   mv $PWD/output/{cki-amqp-bridge.*,nonprod-cki-amqp-bridge.*,cki-umb-messenger-*,nonprod-cki-umb-messenger-*} ${CREDENTIALS_REPO}/certs
   ```
   <!-- markdownlint-restore -->

1. Generate the `pem` file by concatenating the `crt` and `key` files, for each
   of the accounts.

   ```bash
   cat ${SA_NAME}.crt ${SA_NAME}.key > ${SA_NAME}.pem
   ```

1. Add the contents of the `pem` files to the [secrets.yml] file.

   <!-- markdownlint-disable line-length -->
   ```bash
   credentials/ $ export CKI_AMQP_BRIDGE_DEV_PEM=$(cat certs/nonprod-cki-amqp-bridge.pem | base64 -i -w 64 | tr -d '\n')
   credentials/ $ export CKI_AMQP_BRIDGE_PROD_PEM=$(cat certs/cki-amqp-bridge.pem | base64 -i -w 64 | tr -d '\n')
   credentials/ $ export CKI_UMB_MESSENGER_DEV_PEM=$(cat certs/nonprod-cki-umb-messenger.pem | base64 -i -w 64 | tr -d '\n')
   credentials/ $ export CKI_UMB_MESSENGER_PROD_PEM=$(cat certs/cki-umb-messenger.pem | base64 -i -w 64 | tr -d '\n')
   deployment-all/ $ ./encrypt_secret.sh secrets.yml
   Appending to secrets.yml...
   Variable name: CKI_AMQP_BRIDGE_DEV_PEM
   Secret value (not echoed, empty takes from env): [ENTER]
   Variable name: CKI_AMQP_BRIDGE_PROD_PEM
   Secret value (not echoed, empty takes from env): [ENTER]
   Variable name: CKI_UMB_MESSENGER_DEV_PEM
   Secret value (not echoed, empty takes from env): [ENTER]
   Variable name: CKI_UMB_MESSENGER_PROD_PEM
   Secret value (not echoed, empty takes from env): [ENTER]
   ```
   <!-- markdownlint-restore -->

[UMB Getting Started]: https://source.redhat.com/groups/public/enterprise-services-platform/it_platform_wiki/unified_message_bus_umb_getting_started
[How to: Request UMB Certificates]: https://source.redhat.com/groups/public/enterprise-services-platform/it_platform_wiki/how_to_request_a_messaging_client_certificate_from_rhcs
[Accessing Shared Services]: https://source.redhat.com/groups/public/identity-access-management/identity__access_management_wiki/ldap_service_accounts__certificates_for_accessing_shared_services
[IT-IAM Utilities]: https://gitlab.corp.redhat.com/it-iam/utility
[credentials repository]: https://gitlab.cee.redhat.com/cki-project/credentials
[secrets.yml]: https://gitlab.cee.redhat.com/cki-project/deployment-all/-/blob/main/secrets.yml
