---
title: Creating new repositories
description: Want to create a new repo? Start here.
---

## Determining the correct group

The top-level [cki-project] group is only for production repositories.
Because of that, repository creation is disabled by default for this group.
If you want to create a temporary or experimental repository, please create
it in the [experimental] subgroup instead.
If you really need to create a new production repository, temporarily enable
repository creation in the [group settings].

## Clone the template project

**For all groups, new repositories for Python projects must derive from the
[template project].**

Go to the [New project] page in the [cki-project] group. Click
on `Create from template` → `Group` → `general` → `Use template`, and select
`Public` visibility.

Use a reasonable project slug without underscores (use dashes instead) and
leave the project name empty.

[cki-project]: https://gitlab.com/cki-project/
[experimental]: https://gitlab.com/cki-project/experimental/
[group settings]: https://gitlab.com/groups/cki-project/-/edit
[template project]: https://gitlab.com/cki-project/templates/general
[New project]: https://gitlab.com/projects/new?namespace_id=3970123
